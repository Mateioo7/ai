# -*- coding: utf-8 -*-

import time

import pygame

from utils import WHITE, v, GREEN


class Gui:
    def __init__(self, controller):
        self.__controller = controller
        self.dimension = (400, 400)

    def get_init_pygame_screen(self):
        # init the pygame
        pygame.init()
        logo = pygame.image.load("resources/logo32x32.png")
        pygame.display.set_icon(logo)
        pygame.display.set_caption("drone exploration with EA")

        # create a surface on screen that has the size of 800 x 480
        screen = pygame.display.set_mode(self.dimension)
        screen.fill(WHITE)
        return screen

    def show_map(self):
        # closes the pygame
        running = True

        screen = self.get_init_pygame_screen()
        map_image = self.__controller.get_map_image()
        screen.blit(map_image, (0, 0))
        pygame.display.flip()

        # loop for events
        while running:
            # event handling, gets all event from the event queue
            for event in pygame.event.get():
                # only do something if the event is of type QUIT
                if event.type == pygame.QUIT:
                    # change the value to False, to exit the main loop
                    running = False
        pygame.quit()

    def move_drone_on_path(self, path, speed=1, markSeen=True):
        # animation of a drone on a path
        current_map = self.__controller.get_current_map()
        screen = self.get_init_pygame_screen()
        drone_image = pygame.image.load("resources/drona.png")

        running = True
        drone_finished = False
        while running:
            # event handling, gets all event from the event queue
            for event in pygame.event.get():
                # only do something if the event is of type QUIT
                if event.type == pygame.QUIT:
                    # change the value to False, to exit the main loop
                    running = False

            if not drone_finished:
                for i in range(len(path)):
                    screen.blit(self.__controller.get_map_image(), (0, 0))

                    if markSeen:
                        brick = pygame.Surface((20, 20))
                        brick.fill(GREEN)
                        for j in range(i + 1):
                            for var in v:
                                x = path[j][0]
                                y = path[j][1]
                                while ((0 <= x + var[0] < current_map.n and
                                        0 <= y + var[1] < current_map.m) and
                                       current_map.surface[x + var[0]][y + var[1]] != 1):
                                    x = x + var[0]
                                    y = y + var[1]
                                    screen.blit(brick, (y * 20, x * 20))

                    screen.blit(drone_image, (path[i][1] * 20, path[i][0] * 20))
                    pygame.display.flip()
                    time.sleep(0.5 * speed)

                drone_finished = True

        pygame.quit()
