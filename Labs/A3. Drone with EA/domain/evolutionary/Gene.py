from random import randint


# the glass gene can be replaced with int or float, or other types
# depending on your problem's representation
class Gene:
    def __init__(self):
        # random initialise the gene according to the representation
        # 1 - up, 2 - right, 3 - down, 4 - left
        self.direction = randint(1, 4)
