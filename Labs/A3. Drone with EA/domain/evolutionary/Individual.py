from random import *

from domain.evolutionary.Gene import Gene


class Individual:
    def __init__(self, chromosome_size=0):
        self.__chromosome_size = chromosome_size
        self.__chromosome = [Gene() for _ in range(self.__chromosome_size)]
        self.__fitness = None

    @property
    def fitness(self):
        return self.__fitness

    def compute_fitness(self, map, drone):
        """
        Computes the fitness (quality) of an individual.
        In our case, how many valid squares are found by a drone before its battery depletes (the amount of steps a
        drone can make before it runs out of battery is reflected by the chromosome size).

        :param map: the map a drone will explore
        :param drone: the drone
        """

        self.__fitness = 0
        positions_found_by_sensors = []
        direction_variances = [[-1, 0], [0, 1], [1, 0], [0, -1]]

        # for testing purposes using test1.map from resources => computed fitness should be 13
        # path = [[1, 19], [2, 19], [3, 19], [3, 18], [3, 17]]
        path = self.compute_path(drone)

        for position in path:
            if self.position_is_valid(map, position):
                if position not in positions_found_by_sensors:
                    positions_found_by_sensors.append(position)
                    self.__fitness += 1

                for direction_variance in direction_variances:
                    adjacent_position_in_direction = [position[0] + direction_variance[0],
                                                      position[1] + direction_variance[1]]

                    while self.position_is_valid(map, adjacent_position_in_direction):
                        if adjacent_position_in_direction not in positions_found_by_sensors:
                            positions_found_by_sensors.append(adjacent_position_in_direction)
                            self.__fitness += 1

                        adjacent_position_in_direction = [adjacent_position_in_direction[0] + direction_variance[0],
                                                          adjacent_position_in_direction[1] + direction_variance[1]]
            else:
                break

    def position_is_valid(self, map, position):
        return 0 <= position[0] <= map.n - 1 and 0 <= position[1] <= map.m - 1 and \
               map.surface[position[0], position[1]] == 0

    def get_valid_path(self, map, drone):
        path = self.compute_path(drone)

        valid_path = []
        for position in path:
            if self.position_is_valid(map, position):
                valid_path.append(position)
            else:
                break
        return valid_path

    def compute_path(self, drone):
        path = [[drone.x, drone.y]]
        for gene in self.__chromosome:
            if gene.direction == 1:
                path.append([path[-1][0] - 1, path[-1][1]])
            elif gene.direction == 2:
                path.append([path[-1][0], path[-1][1] + 1])
            elif gene.direction == 3:
                path.append([path[-1][0] + 1, path[-1][1]])
            elif gene.direction == 4:
                path.append([path[-1][0], path[-1][1] - 1])
        return path

    def mutate(self, mutateProbability=0.04):
        if random() < mutateProbability:
            # perform a mutation with respect to the representation
            random_gene_index = randint(0, self.__chromosome_size - 1)
            self.__chromosome[random_gene_index] = Gene()

    def crossover(self, other_parent, crossoverProbability=0.8):
        offspring1, offspring2 = Individual(self.__chromosome_size), Individual(self.__chromosome_size)
        if random() < crossoverProbability:
            # perform the crossover between the self and the otherParent
            crossover_point = randint(0, self.__chromosome_size - 1)
            # until crossover point give to offspring1 the other parent's genes and
            # to offspring2 the current parent's genes
            for gene_index in range(0, crossover_point):
                offspring1.__chromosome[gene_index] = other_parent.__chromosome[gene_index]
                offspring2.__chromosome[gene_index] = self.__chromosome[gene_index]
            # after crossover point give to offspring1 the current parent's genes and
            # to offspring2 the other parent's genes
            for gene_index in range(crossover_point, self.__chromosome_size):
                offspring1.__chromosome[gene_index] = self.__chromosome[gene_index]
                offspring2.__chromosome[gene_index] = other_parent.__chromosome[gene_index]

        return offspring1, offspring2
