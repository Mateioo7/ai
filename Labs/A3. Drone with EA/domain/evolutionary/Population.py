import numpy

from domain.evolutionary.Individual import Individual


class Population:
    def __init__(self, number_of_individuals=0, chromosome_size=0):
        self.__number_of_individuals = number_of_individuals
        self.__individuals = [Individual(chromosome_size) for _ in range(number_of_individuals)]

    def get_individuals(self):
        return self.__individuals

    def set_population(self, new_population):
        self.__individuals = new_population

    def add_individual(self, individual):
        self.__individuals.append(individual)

    def evaluate(self, map, drone):
        # evaluates the population
        for individual in self.__individuals:
            individual.compute_fitness(map, drone)

    def selection(self, k=0):
        # perform a selection of k individuals from the population
        # and returns that selection
        most_fit_sorted_population = sorted(self.__individuals,
                                            key=lambda individual: individual.fitness, reverse=True)
        most_fit_selection = []
        for individual_index in range(0, k):
            most_fit_selection.append(most_fit_sorted_population[individual_index])

        return most_fit_selection

    def get_fitness_list(self, map, drone):
        self.evaluate(map, drone)

        fitness_list = []
        for individual in self.__individuals:
            fitness_list.append(individual.fitness)
        return fitness_list

    def get_statistics_data(self, map, drone):
        fitness_list = self.get_fitness_list(map, drone)
        return [numpy.average(fitness_list), numpy.std(fitness_list)]
