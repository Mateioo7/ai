import os
import pickle
from random import random

import numpy as np
import pygame

from utils import BLUE, WHITE


class Map:
    def __init__(self, n=20, m=20):
        self.n = n
        self.m = m
        self.surface = self.reset_surface()

    def randomMap(self, fill=0.2):
        self.surface = self.reset_surface()
        for i in range(self.n):
            for j in range(self.m):
                if random() <= fill:
                    self.surface[i][j] = 1

    def reset_surface(self):
        return np.zeros((self.n, self.m))

    def __str__(self):
        string = ""
        for i in range(self.n):
            for j in range(self.m):
                string = string + str(int(self.surface[i][j]))
            string = string + "\n"
        return string

    def saveMap(self, file_path="resources/test.map"):
        if not os.path.exists(file_path):
            os.mknod(file_path)

        with open(file_path, 'wb') as f:
            pickle.dump(self, f)
            f.close()

    def loadMap(self, numFile):
        with open(numFile, "rb") as f:
            dummy = pickle.load(f)
            self.n = dummy.n
            self.m = dummy.m
            self.surface = dummy.surface
            f.close()

    def image(self, colour=BLUE, background=WHITE):
        image = pygame.Surface((400, 400))
        brick = pygame.Surface((20, 20))
        brick.fill(colour)
        image.fill(background)
        for i in range(self.n):
            for j in range(self.m):
                if self.surface[i][j] == 1:
                    image.blit(brick, (j * 20, i * 20))

        return image
