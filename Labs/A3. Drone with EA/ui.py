from controller import *
from gui import *


class Ui:
    def __init__(self):
        self.__controller = Controller()
        self.__gui = Gui(self.__controller)

        self.__fitness_averages_on_runs = None
        self.__fitness_standard_deviations_on_runs = None
        self.__most_fit_individual_valid_path = None

    def run(self):
        self.__controller.load_map()

        map_menu_options = {
            "1": self.load_random_map,
            "2": self.load_map,
            "3": self.save_map,
            "4": self.__gui.show_map
        }

        ea_menu_options = {
            "1": self.__controller.load_parameters_from_file,
            "2": self.run_solver,
            "3": self.show_statistics,
            "4": self.view_drone_move_on_path
        }

        running = True
        on_map_menu = True
        while running:
            if on_map_menu:
                self.show_map_menu()
                option = input("Choose option: ")

                if option == "0":
                    running = False
                elif option == "5":
                    on_map_menu = False
                else:
                    map_menu_options[option]()
            else:
                self.show_ea_menu()
                option = input("Choose option: ")

                if option == "0":
                    running = False
                else:
                    ea_menu_options[option]()

    def view_drone_move_on_path(self):
        # path = [[1, 19], [2, 19], [3, 19], [3, 18], [3, 17]]
        self.__gui.move_drone_on_path(self.__most_fit_individual_valid_path)

    def run_solver(self):
        start_time = time.time()

        self.__fitness_averages_on_runs, self.__fitness_standard_deviations_on_runs, \
        self.__most_fit_individual_valid_path = self.__controller.solver()

        elapsed_time = time.time() - start_time
        print("\n--- %.2f seconds elapsed ---" % elapsed_time)

    def show_statistics(self):
        iteration_index = list(range(1, len(self.__fitness_averages_on_runs) + 1))

        plotter.xlabel("run number")

        plotter.plot(iteration_index, self.__fitness_averages_on_runs, label='fitness average')
        plotter.plot(iteration_index, self.__fitness_standard_deviations_on_runs, label='fitness standard deviation')

        plotter.legend()
        plotter.show()

    def load_random_map(self):
        fill_chance = input("Give fill chance for a square (0.2 if left empty):")
        if fill_chance == "":
            self.__controller.load_random_map()
        else:
            self.__controller.load_random_map(float(fill_chance))

    def save_map(self):
        path = self.get_file_path()
        self.__controller.save_map(path)

    def load_map(self):
        path = self.get_file_path()
        self.__controller.load_map(path)

    def get_file_path(self):
        return "resources/" + input("Give file name: ")

    def show_map_menu(self):
        print("")
        print("0. Exit.")
        print("1. Load random map.")
        print("2. Load existing map (default loaded file: test1.map).")
        print("3. Save current map.")
        print("4. Visualise map.")
        print("5. Go to evolutionary algorithm menu.")

    def show_ea_menu(self):
        print("")
        print("0. Exit.")
        print("1. Setup parameters.")
        print("2. Run the solver.")
        print("3. Visualise the statistics.")
        print("4. View the drone moving on a path.")