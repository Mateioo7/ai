# DON'T OPTIMIZE IMPORTS, WITHOUT DOMAIN.MAP IT CRASHES SOMEHOW
from domain.Map import Map
from ui import Ui


def main():
    ui = Ui()
    ui.run()


if __name__ == "__main__":
    main()

