from random import randint

from domain.Drone import Drone
from domain.Map import Map
from domain.evolutionary.Population import Population


class Repository:
    def __init__(self):
        self.__populations = []
        self.map = Map()
        self.drone = self.get_drone_in_random_position()

    def get_most_fit_individual_valid_path(self):
        self.evaluate_last_population()
        most_fit_individual = self.get_last_population().selection(1)[0]
        return most_fit_individual.get_valid_path(self.map, self.drone)
        
    def add_random_population(self, number_of_individuals, chromosome_size):
        self.__populations.append(Population(number_of_individuals, chromosome_size))

    def add_population(self, population, chromosome_size):
        new_population = Population(len(population), chromosome_size)
        new_population.set_population(population)
        self.__populations.append(new_population)

    def get_last_population(self):
        return self.__populations[-1]

    def get_statistics_data_from_last_population(self):
        return self.get_last_population().get_statistics_data(self.map, self.drone)

    def evaluate_last_population(self):
        self.get_last_population().evaluate(self.map, self.drone)

    def add_individual_to_population(self, individual, population):
        population.add_individual(individual)

    def load_random_map(self, fill):
        self.map.randomMap(fill)

    def save_map(self, file_name):
        self.map.saveMap(file_name)

    def load_map(self, file_name):
        self.map.loadMap(file_name)

    def get_map_image(self, color, background):
        return self.map.image(color, background)

    def get_drone_in_random_position(self):
        x, y = self.get_random_valid_position()
        return Drone(x, y)

    def get_random_valid_position(self):
        x = randint(0, self.map.n - 1)
        y = randint(0, self.map.m - 1)

        while self.map.surface[x][y] != 0:
            x = randint(0, self.map.n - 1)
            y = randint(0, self.map.m - 1)

        return x, y
