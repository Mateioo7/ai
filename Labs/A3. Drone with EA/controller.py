import time
from random import randint, seed

import matplotlib.pyplot as plotter
import numpy

from repository import Repository
from utils import BLUE, WHITE


class Controller:
    def __init__(self):
        self.__repository = Repository()
        self.__number_of_runs = None
        self.__number_of_iterations_per_run = None
        self.__population_size = None
        self.__chromosome_size = None
        self.__crossover_probability = None
        self.__mutation_probability = None

        # statistics
        self.__fitness_averages_on_runs = None
        self.__fitness_standard_deviations_on_runs = None

    def get_current_map(self):
        return self.__repository.map

    def load_random_map(self, fill=0.2):
        self.__repository.load_random_map(fill)

    def load_map(self, file_name="resources/test1.map"):
        self.__repository.load_map(file_name)

    def save_map(self, file_name="resources/test.map"):
        self.__repository.save_map(file_name)

    def get_map_image(self, color=BLUE, background=WHITE):
        return self.__repository.get_map_image(color, background)

    def compute_one_iteration(self):
        population = self.__repository.get_last_population()
        individuals = population.get_individuals()
        parents = individuals[0:len(individuals) // 2]
        number_of_pairs = len(parents) // 2
        crossovers_made = []

        for pair_index in range(number_of_pairs):
            # we select 2 random parents
            parent1 = parents[randint(0, len(parents) - 1)]
            parent2 = parents[randint(0, len(parents) - 1)]
            while parent2 != parent1:
                parent2 = parents[randint(0, len(parents) - 1)]

            if [parent1, parent2] not in crossovers_made:
                crossovers_made.append([parent1, parent2])
                # we create offsprings from parents crossover
                offspring1, offspring2 = parent1.crossover(parent2, self.__crossover_probability)
                # apply mutation on offsprings
                offspring1.mutate(self.__mutation_probability)
                offspring2.mutate(self.__mutation_probability)
                # we add the offsprings to the population
                self.__repository.add_individual_to_population(offspring1, population)
                self.__repository.add_individual_to_population(offspring2, population)
                # population.add_individual(offspring1)
                # population.add_individual(offspring1)

        # we keep the most fit individuals after iteration
        # population.set_population(most_fit_individuals)

        population.evaluate(self.__repository.map, self.__repository.drone)
        new_most_fit_individuals = population.selection(self.__population_size)
        self.__repository.add_population(new_most_fit_individuals, self.__chromosome_size)


    def run(self, fitness_averages_on_runs, fitness_standard_deviations_on_runs):
        # args - list of parameters needed in order to run the algorithm

        # until stop condition
        #    perform an iteration
        #    save the information need it for the statistics
        fitness_averages = []
        fitness_standard_deviations = []

        # return the results and the info for statistics
        for iteration_index in range(0, self.__number_of_iterations_per_run):
            self.compute_one_iteration()

            statistics_data = self.__repository.get_statistics_data_from_last_population()
            fitness_averages.append(statistics_data[0])
            fitness_standard_deviations.append(statistics_data[1])

        fitness_average_on_run = numpy.average(fitness_averages)
        fitness_standard_deviation_on_run = numpy.std(fitness_averages)

        fitness_averages_on_runs.append(fitness_average_on_run)
        fitness_standard_deviations_on_runs.append(fitness_standard_deviation_on_run)

    def solver(self):
        # args - list of parameters needed in order to run the solver

        # create the population,
        # run the algorithm
        # return the results and the statistics

        fitness_averages_on_runs = []
        fitness_standard_deviations_on_runs = []

        for run_index in range(0, self.__number_of_runs):
            seed(run_index)

            self.__repository.add_random_population(self.__population_size, self.__chromosome_size)
            self.__repository.evaluate_last_population()

            self.run(fitness_averages_on_runs, fitness_standard_deviations_on_runs)

        most_fit_individual_valid_path = self.__repository.get_most_fit_individual_valid_path()

        return fitness_averages_on_runs, fitness_standard_deviations_on_runs, most_fit_individual_valid_path

    def load_parameters_from_file(self):
        with open('resources/settings.txt') as f:
            lines = f.readlines()

        for line_index in range(0, len(lines)):
            if line_index == 0:
                self.__number_of_runs = int(lines[line_index])
            elif line_index == 1:
                self.__number_of_iterations_per_run = int(lines[line_index])
            elif line_index == 2:
                self.__population_size = int(lines[line_index])
            elif line_index == 3:
                self.__chromosome_size = int(lines[line_index])
            elif line_index == 4:
                self.__crossover_probability = float(lines[line_index])
            elif line_index == 5:
                self.__mutation_probability = float(lines[line_index])
