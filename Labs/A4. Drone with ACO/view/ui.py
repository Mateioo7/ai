from controller.controller import *
from view.gui import Gui


class Ui:
    def __init__(self):
        self.__controller = Controller()
        self.__gui = Gui(self.__controller)

        self.__best_ant = None

    def run(self):
        self.__controller.load_map()

        menu_options = {
            "1": self.load_random_map,
            "2": self.load_map,
            "3": self.save_map,
            "4": self.__gui.show_map,
            "5": self.run_aco_solver,
            "6": self.view_drone_move_on_path
        }

        running = True
        while running:
            self.show_menu()
            option = input("Choose option: ")

            if option == "0":
                running = False
            else:
                menu_options[option]()

    def run_aco_solver(self):
        iteration_index, ant_index, self.__best_ant = self.__controller.aco_solver()
        print("Meet best ant, Fănică" + ", ant " + str(ant_index) + " from iteration " + str(iteration_index) +
              ". His stats:")
        print("He walked " + str(len(self.__best_ant.path)) + " steps following the route " +
              str(self.__best_ant.squares_surveilled_by_sensors) +
              " and thanks to him, the reached sensors surveilled " + str(self.__best_ant.fitness()) + " total area.")

    def view_drone_move_on_path(self):
        # path = [[1, 19], [2, 19], [3, 19], [3, 18], [3, 17]]
        self.__gui.move_drone_on_path(self.__best_ant.path)

    def load_random_map(self):
        fill_chance = input("Give fill chance for a square (0.2 if left empty):")
        if fill_chance == "":
            self.__controller.load_random_map()
        else:
            self.__controller.load_random_map(float(fill_chance))

    def save_map(self):
        path = self.get_file_path()
        self.__controller.save_map(path)

    def load_map(self):
        path = self.get_file_path()
        self.__controller.load_map(path)

    def get_file_path(self):
        return "resources/" + input("Give file name: ")

    def show_menu(self):
        print("")
        print("0. Exit.")
        print("1. Load random map.")
        print("2. Load existing map (default loaded file: test1.map).")
        print("3. Save current map.")
        print("4. Visualise map.")
        print("5. Run ACO solver.")
        print("6. View the drone moving on the best path.")
