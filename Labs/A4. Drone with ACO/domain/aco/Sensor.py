from utils import direction_variances


class Sensor:
    def __init__(self, x_position, y_position):
        self.x_position = x_position
        self.y_position = y_position
        self.number_of_squares_discovered = {0: 0, 1: 0, 2: 0, 3: 0, 4: 0, 5: 0}

    def get_optimal_energy_needed(self, map):
        """
        :param map: the map in which the sensor is placed
        :return: the minimum energy needed to discover the maximum total area
        """
        return max(self.number_of_squares_discovered, key=self.number_of_squares_discovered.get)

    def discover_squares(self, map):
        for energy_value in range(1, 6):
            for direction_variance in direction_variances:
                energy_value_aux = energy_value
                adjacent_position_in_direction = [self.x_position + direction_variance[0],
                                                  self.y_position + direction_variance[1]]

                while self.position_is_valid(map, adjacent_position_in_direction) and energy_value_aux > 0:
                    self.number_of_squares_discovered[energy_value] += 1
                    energy_value_aux -= 1

                    adjacent_position_in_direction = [adjacent_position_in_direction[0] + direction_variance[0],
                                                      adjacent_position_in_direction[1] + direction_variance[1]]

    def position_is_valid(self, map, position):
        return 0 <= position[0] < map.n and 0 <= position[1] < map.m and \
               map.surface[position[0], position[1]] == 0
