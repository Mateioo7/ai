from random import random


class Ant:
    def __init__(self, energy):
        self.current_sensor_index = 0
        self.energy = energy
        self.path = []
        self.visited_sensors_indexes = [0]
        self.squares_surveilled_by_sensors = {}

    def add_visited_sensor_index(self, new_sensor_index):
        self.visited_sensors_indexes.append(new_sensor_index)

    def add_path(self, path):
        for p in path:
            self.path.append(p)

    def choose_unvisited_sensor_index(self, probability_ranges):
        # float value in 0 <= float_value < 1
        random_value = random()
        for i in range(len(probability_ranges) - 1):
            if probability_ranges[i] > random_value >= probability_ranges[i + 1]:
                return i
        # if it's not between 2 values from the list, it's the last one
        return len(probability_ranges) - 1

    def total_distance(self):
        return len(self.path)

    def fitness(self):
        # how good is (quality level) our ant based on the assignment task
        # => how many squares were surveilled by the reached and charged sensors before its energy ran out
        return sum(self.squares_surveilled_by_sensors.values())
