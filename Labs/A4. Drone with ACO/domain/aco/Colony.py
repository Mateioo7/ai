from domain.aco.Ant import Ant


class Colony:
    def __init__(self, number_of_ants, energy):
        self.__number_of_individuals = number_of_ants
        self.__ants = [Ant(energy) for _ in range(number_of_ants)]

    def get_ants(self):
        return self.__ants

    def send_ants_home(self, number_of_ants, energy):
        self.__ants = [Ant(energy) for _ in range(number_of_ants)]
