class AStarPosition:
    def __init__(self, x, y, previous_a_star_position=None):
        self.x = x
        self.y = y
        self.previous_a_star_position = previous_a_star_position

        self.total_cost = 0
        self.distance_from_start = 0
        self.estimated_distance_to_end = 0

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y

    def __lt__(self, other):
        return self.total_cost < other.total_cost

    def __gt__(self, other):
        return self.total_cost > other.total_cost
