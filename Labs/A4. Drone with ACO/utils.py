# Creating some colors
BLUE = (0, 0, 255)
GRAY_BLUE = (50, 120, 120)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)

# define directions
UP = 0
DOWN = 2
LEFT = 1
RIGHT = 3

# define indexes variations
direction_variances = [[-1, 0], [0, 1], [1, 0], [0, -1]]

# define map size
mapLength = 20

# ACO problem variables
ant_initial_position = [0, 19]
ant_energy = 50
colony_size = 100  # how many ants (drones) in a colony (drone party)
food_positions = [[18, 16], [2, 4], [6, 6], [12, 3], [18, 8]]  # sensor's positions
number_of_iterations = 10  # how many times to release a colony to search for shortest path
alpha, beta = 1.9, 0.9  # weights needed for pheromone computation
evaporation_coefficient = 0.5  # coefficient used for evaporation
