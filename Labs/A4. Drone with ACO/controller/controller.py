from heapq import heapify, heappush, heappop

import numpy

from domain.AStarPosition import AStarPosition
from domain.aco.Sensor import Sensor
from repository.repository import Repository
from utils import BLUE, WHITE, direction_variances, food_positions, colony_size, ant_energy, \
    ant_initial_position, number_of_iterations, alpha, beta, evaporation_coefficient


class Controller:
    def __init__(self):
        self.__repository = Repository()
        self.__sensors = []
        # paths from each sensor to each sensor to avoid repeated a_star_search calls
        self.__sensors_paths = []
        # how probably is to visit a sensor based on the cost
        self.__visibility_matrix = None
        # how likely is for an ant to go to a next valid sensor based on visibility and pheromone
        self.__probability_ranges = None
        self.__pheromone_matrix = None
        self.__alpha, self.__beta = alpha, beta
        self.__evaporation_coefficient = evaporation_coefficient

    def compute_aco_iteration(self):
        colony = self.__repository.get_last_colony()

        for ant in colony.get_ants():
            visibility_matrix = self.__visibility_matrix.copy()
            while ant.energy > 0 and len(ant.visited_sensors_indexes) < len(self.__sensors):
                self.update_visibility_matrix(visibility_matrix, ant.current_sensor_index)
                self.update_probability_ranges(ant)

                next_sensor_probability_index = ant.choose_unvisited_sensor_index(self.__probability_ranges)
                next_sensor_index = self.get_nth_unvisited_index(ant, next_sensor_probability_index)
                # we update the ant's path only if it didn't reach all the destinations
                if next_sensor_index is not None:
                    new_path = self.__sensors_paths[ant.current_sensor_index][next_sensor_index]

                    ant.add_path(new_path)
                    ant.energy -= len(new_path)

                    # if ant can't reach next sensor :<, it goes back when energy was 0
                    if ant.energy < 0:
                        ant.path = ant.path[0:len(ant.path) - abs(ant.energy)]
                        ant.energy = 0
                    elif ant.energy > 0:
                        optimal_energy_required \
                            = self.__sensors[next_sensor_index].get_optimal_energy_needed(self.__repository.map)
                        ant.energy -= optimal_energy_required
                        # if ant can't charge sensor with the optimal energy value :<, it charges it with what it has
                        if ant.energy < 0:
                            actual_energy = ant.energy + optimal_energy_required
                            ant.squares_surveilled_by_sensors[next_sensor_index] = \
                                self.__sensors[next_sensor_index].number_of_squares_discovered[actual_energy]
                            ant.energy = 0
                        else:
                            ant.squares_surveilled_by_sensors[next_sensor_index] = \
                                self.__sensors[next_sensor_index].number_of_squares_discovered[optimal_energy_required]

                        ant.current_sensor_index = next_sensor_index
                        ant.add_visited_sensor_index(ant.current_sensor_index)

        self.update_pheromone_matrix(colony)
        best_ant = self.get_best_ant()

        colony.send_ants_home(colony_size, ant_energy)

        return best_ant

    def get_best_ant(self):
        ants = self.__repository.get_last_colony().get_ants()

        ants_with_fitness = [[i, ants[i].fitness()] for i in range(len(ants))]
        best_ant_index = min(ants_with_fitness)[0]

        return best_ant_index, ants[best_ant_index]

    def aco_solver(self):
        self.create_sensors()
        self.discover_sensors_squares()
        self.compute_sensors_paths()

        self.init_visibility_matrix()
        self.init_pheromone_matrix()

        self.__repository.add_colony(colony_size, ant_energy)

        best_ant = None
        best_iteration = -1
        best_ant_index = -1
        for iteration_index in range(number_of_iterations):
            if iteration_index == 0:
                best_ant_index, best_ant = self.compute_aco_iteration()
                best_iteration = iteration_index
            else:
                ant_index, ant = self.compute_aco_iteration()
                if len(ant.path) <= len(best_ant.path):
                    best_ant = ant
                    best_ant_index = ant_index
                    best_iteration = iteration_index

        return best_iteration, best_ant_index, best_ant

    def compute_sensors_paths(self):
        self.__sensors_paths = [[] for _ in range(len(self.__sensors))]
        for i in range(len(self.__sensors)):
            for j in range(len(self.__sensors)):
                # without first one as it is the initial position
                self.__sensors_paths[i].append(self.a_star_search_path(self.__sensors[i].x_position,
                                                                       self.__sensors[i].y_position,
                                                                       self.__sensors[j].x_position,
                                                                       self.__sensors[j].y_position)[1:])

    def get_nth_unvisited_index(self, ant, probability_index):
        """
        Returns the corresponding unvisited sensor index for the randomly chosen index from the probabilities ranges.

        :param ant: the ant
        :param probability_index: randomly chosen unvisited sensor index from the probabilities range
        :return: the index of the randomly chosen sensor
        """
        # we add 1 since probability_index is from [0, len(self.__sensors))
        probability_index += 1
        current_unvisited_sensor_index = 0
        for sensor_index in range(1, len(self.__sensors)):
            if sensor_index not in ant.visited_sensors_indexes:
                current_unvisited_sensor_index += 1

                if current_unvisited_sensor_index == probability_index:
                    return sensor_index

    def update_pheromone_matrix(self, colony):
        # we compute the evaporation
        for i in range(len(self.__sensors)):
            for j in range(len(self.__sensors)):
                self.__pheromone_matrix[i][j] = (1 - self.__evaporation_coefficient) * self.__pheromone_matrix[i][j]
        # we add the additional pheromone from our ants
        for ant in colony.get_ants():
            visited = ant.visited_sensors_indexes
            for i in range(len(visited) - 1):
                self.__pheromone_matrix[visited[i]][visited[i + 1]] += (1 / ant.total_distance())

    def create_sensors(self):
        # we also represent the starting point as a sensor - the departure sensor
        self.__sensors.append(Sensor(ant_initial_position[0], ant_initial_position[1]))
        for sensor_position in food_positions:
            self.__sensors.append(Sensor(sensor_position[0], sensor_position[1]))

    def discover_sensors_squares(self):
        for sensor in self.__sensors:
            sensor.discover_squares(self.__repository.map)

    def update_probability_ranges(self, ant):
        current_sensor_index = ant.current_sensor_index
        numerators = []
        for j in range(1, len(self.__sensors)):
            # we ignore the probability to reach the already visited sensors
            if j not in ant.visited_sensors_indexes:
                numerators.append(self.__pheromone_matrix[current_sensor_index][j] ** self.__alpha
                                  *
                                  self.__visibility_matrix[current_sensor_index][j] ** self.__beta)
        denominator = sum(numerators)

        probabilities = []
        for numerator_index in range(len(numerators)):
            probabilities.append(numerators[numerator_index] / denominator)

        self.__probability_ranges = []
        # ex: [0.76, 0.19, 0.05] => [1.0, 0.24, 0.05]
        # then if 0.24 < random value between [0 and 1) <= 1 =>
        # => sensor with probability 0.76 is chosen
        for i in range(len(probabilities)):
            self.__probability_ranges.append(sum(probabilities[i:]))

    def update_visibility_matrix(self, visibility_matrix, current_sensor_index):
        for i in range(len(self.__sensors)):
            visibility_matrix[i][current_sensor_index] = 0

    def init_visibility_matrix(self):
        self.__visibility_matrix = numpy.zeros((len(self.__sensors), len(self.__sensors)))
        for i in range(len(self.__sensors)):
            for j in range(len(self.__sensors)):
                if i != j:
                    self.__visibility_matrix[i][j] = 1 / len(self.__sensors_paths[i][j])
        # we keep visibility 0 from any sensor to sensor 0 as sensor 0 is the departure sensor
        for i in range(len(self.__sensors)):
            self.__visibility_matrix[i][0] = 0

    def init_pheromone_matrix(self):
        self.__pheromone_matrix = numpy.ones((len(self.__sensors), len(self.__sensors)))

    def get_largest_column_from_pheromone_matrix_row(self, row):
        largest_value = max(self.__pheromone_matrix[row])
        for j in range(len(self.__sensors)):
            if self.__pheromone_matrix[row][j] == largest_value:
                return j

    def a_star_search_path(self, initial_x, initial_y, final_x, final_y):
        initial_position = AStarPosition(initial_x, initial_y, None)
        final_position = AStarPosition(final_x, final_y, None)

        positions_with_unvisited_neighbours = []
        positions_with_visited_neighbours = []

        # create the heap (priority queue) for taking the lowest total cost position
        heapify(positions_with_unvisited_neighbours)
        heappush(positions_with_unvisited_neighbours, initial_position)

        while len(positions_with_unvisited_neighbours) > 0:
            # we pop the drone position with the least total cost
            current_position = heappop(positions_with_unvisited_neighbours)

            # if the final position is reached, we return the reversed path
            if current_position == final_position:
                return self.a_star_reversed_path(current_position)

            neighbours_positions = []
            # we find the valid neighbours positions
            for direction_variance in direction_variances:
                neighbour_position = [current_position.x + direction_variance[0],
                                      current_position.y + direction_variance[1]]

                if self.position_is_valid(neighbour_position):
                    neighbours_positions.append(AStarPosition(neighbour_position[0], neighbour_position[1],
                                                              current_position))

            for neighbour_position in neighbours_positions:
                # if position was already inspected, we skip it
                if neighbour_position in positions_with_visited_neighbours:
                    continue
                # we set the position's properties
                neighbour_position.distance_from_start = current_position.distance_from_start + 1
                neighbour_position.estimated_distance_to_end = self.manhattan_distance(neighbour_position.x,
                                                                                       neighbour_position.y,
                                                                                       final_x, final_y)
                neighbour_position.total_cost = neighbour_position.distance_from_start \
                                                + neighbour_position.estimated_distance_to_end
                # if position is still under inspection and the new total cost is greater than before, we skip it
                if len([position for position in positions_with_unvisited_neighbours
                        if neighbour_position == position and \
                           neighbour_position.distance_from_start > position.distance_from_start]):
                    continue

                # if the position was not inspected or was found a lower total cost, we add it for more inspection
                heappush(positions_with_unvisited_neighbours, neighbour_position)
            # we mark the current position as inspected
            positions_with_visited_neighbours.append(current_position)

        # no path is possible as a* algorithm guarantees the best possible solution if any solution exists
        return []

    def a_star_reversed_path(self, drone_position):
        path = []
        current_drone_position = drone_position
        while current_drone_position is not None:
            path.append([current_drone_position.x, current_drone_position.y])
            current_drone_position = current_drone_position.previous_a_star_position
        return path[::-1]

    def manhattan_distance(self, initial_x, initial_y, final_x, final_y):
        return abs(initial_x - final_x) + abs(initial_y - final_y)

    def position_is_valid(self, position):
        map = self.__repository.map
        return 0 <= position[0] < map.n and 0 <= position[1] < map.m and map.surface[position[0], position[1]] == 0

    def get_current_map(self):
        return self.__repository.map

    def load_random_map(self, fill=0.2):
        self.__repository.load_random_map(fill)

    def load_map(self, file_name="resources/test1.map"):
        self.__repository.load_map(file_name)

    def save_map(self, file_name="resources/test.map"):
        self.__repository.save_map(file_name)

    def get_map_image(self, color=BLUE, background=WHITE):
        return self.__repository.get_map_image(color, background)
