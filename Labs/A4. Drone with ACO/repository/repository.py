from domain.Map import Map
from domain.aco.Colony import Colony


class Repository:
    def __init__(self):
        self.__colonies = []
        self.map = Map()

    def add_colony(self, number_of_ants, energy):
        self.__colonies.append(Colony(number_of_ants, energy))

    def get_last_colony(self):
        return self.__colonies[-1]

    def load_random_map(self, fill):
        self.map.randomMap(fill)

    def save_map(self, file_name):
        self.map.saveMap(file_name)

    def load_map(self, file_name):
        self.map.loadMap(file_name)

    def get_map_image(self, color, background):
        return self.map.get_image_with_drone_and_sensors(color, background)
