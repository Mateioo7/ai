import glob

import numpy as np
import requests
from PIL import Image

from dataset import ImageClassifierDataset


def main():
    # load the image
    # image1 = Image.open('f1.jpg')
    # # summarize some details about the image
    # print(image1.format)
    # print(image1.mode)
    # print(image1.size)
    # # show the image
    # image1.show()

    # load image as pixel array
    # data = image.imread('f1.jpg')
    # # summarize shape of the pixel array
    # print(data.dtype)
    # print(data.shape)
    # # display the array of pixels as an image
    # pyplot.imshow(data)
    # pyplot.show()

    image2 = Image.open('f1.jpg')
    # report the size of the image
    print(image2.size)
    # create a thumbnail and preserve aspect ratio
    image2.thumbnail((100, 100))
    # report the size of the thumbnail
    print(image2.size)

    # images_list = []
    # for image_file in glob.iglob("/home/matei/Facultate/ai/Labs/A8. Face detection/images/class3/*"):
    #     images_list.append(np.asarray(Image.open(image_file)))
    # dataset = ImageClassifierDataset(images_list)

    for i in range(0, 50):
        response = requests.get("https://fakeface.rest/face/json?gender=male")
        response = response.json()
        print(response["image_url"])
        response = requests.get(response["image_url"])

        file = open("train/men/" + str(i + 1) + ".png", "wb")
        file.write(response.content)
        file.close()

    for i in range(0, 50):
        response = requests.get("https://fakeface.rest/face/json?gender=female")
        response = response.json()
        print(response["image_url"])
        response = requests.get(response["image_url"])

        file = open("train/women/" + str(i + 1) + ".png", "wb")
        file.write(response.content)
        file.close()

    for i in range(0, 50):
        response = requests.get("https://dog.ceo/api/breeds/image/random")
        response = response.json()
        print(response["message"])
        response = requests.get(response["message"])

        file = open("train/dogs/" + str(i + 1) + ".png", "wb")
        file.write(response.content)
        file.close()


main()
