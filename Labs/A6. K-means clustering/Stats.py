class Stats:
    def __init__(self, cluster_label, accuracy, precision, recall, score):
        self.cluster_label = cluster_label
        self.accuracy = accuracy
        self.precision = precision
        self.recall = recall
        self.score = score
