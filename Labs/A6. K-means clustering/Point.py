import math


class Point:
    def __init__(self, given_label, x, y):
        self.given_label = given_label
        self.x = float(x)
        self.y = float(y)
        self.cluster = None

    def get_closest_cluster(self, clusters):
        closest_cluster = clusters[0]

        for i in range(1, len(clusters)):
            if self.euclidean_distance(self.x, self.y, clusters[i].x_mean, clusters[i].y_mean) < \
                    self.euclidean_distance(self.x, self.y, closest_cluster.x_mean, closest_cluster.y_mean):
                closest_cluster = clusters[i]

        return closest_cluster

    def euclidean_distance(self, x1, y1, x2, y2):
        return math.dist((x1, y1), (x2, y2))
