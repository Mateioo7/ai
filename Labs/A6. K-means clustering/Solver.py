import csv
import random

from Cluster import Cluster
from Point import Point


class Solver:
    def __init__(self):
        self.points = self.read_csv()
        self.clusters = [Cluster('A'), Cluster('B'), Cluster('C'), Cluster('D')]
        self.stats = []

    def read_csv(self):
        points = []
        with open('dataset.csv', newline='') as csv_file:
            reader = csv.reader(csv_file, delimiter=' ', quotechar='|')
            line_index = -1
            for row in reader:
                line_index += 1
                # without header
                if line_index == 0:
                    continue

                split_row = row[0].split(',')
                points.append(Point(split_row[0], split_row[1], split_row[2]))

        return points

    def initialize_random_centroids(self):
        random_points = random.sample(self.points, len(self.clusters))

        for i in range(len(self.clusters)):
            self.clusters[i].x_mean = random_points[i].x
            self.clusters[i].y_mean = random_points[i].y

    def update_clusters_label(self):
        for cluster in self.clusters:
            self.update_cluster_label(cluster)

    def update_cluster_label(self, cluster):
        """
        Since centroids were chosen randomly, cluster's label is very likely incorrect after all computations
        and we update it with the correct one based on the given label of the points which is correct.
        """
        label_frequency = {'A': 0, 'B': 0, 'C': 0, 'D': 0}
        for point in cluster.points:
            label_frequency[point.given_label] += 1

        cluster.label = max(label_frequency, key=label_frequency.get)

    def compute_stats(self):
        for cluster in self.clusters:
            self.stats.append(cluster.get_stats(self.points))

    def print_stats(self):
        for stat in self.stats:
            print("Cluster: " + stat.cluster_label + "\n" +
                  "Accuracy: " + str(stat.accuracy) + "\n" +
                  "Precision: " + str(stat.precision) + "\n" +
                  "Recall: " + str(stat.recall) + "\n" +
                  "Score: " + str(stat.score) + "\n")

    def k_means_clustering(self):
        self.initialize_random_centroids()

        centroid_changed = True
        while centroid_changed:
            for point in self.points:
                closest_cluster = point.get_closest_cluster(self.clusters)
                closest_cluster.add_point(point)

                centroid_changed = closest_cluster.update_centroid()

        self.update_clusters_label()
        self.compute_stats()
