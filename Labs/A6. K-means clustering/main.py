from Solver import Solver


def main():
    solver = Solver()
    solver.k_means_clustering()
    solver.print_stats()


main()
