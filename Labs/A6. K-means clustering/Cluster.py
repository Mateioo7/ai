from Stats import Stats


class Cluster:
    def __init__(self, label):
        self.label = label
        self.points = []
        self.x_mean = 0
        self.y_mean = 0

    def add_point(self, point):
        self.points.append(point)

        # we remove the point from previous cluster's list of points
        if point.cluster is not None:
            point.cluster.points.remove(point)

        point.cluster = self

    def update_centroid(self):
        """
        Computes new mean.

        :return: True if mean changed. False if mean didn't change.
        """
        old_x_mean = self.x_mean
        old_y_mean = self.y_mean

        self.x_mean = sum([point.x for point in self.points]) / len(self.points)
        self.y_mean = sum([point.y for point in self.points]) / len(self.points)

        return not (self.almost_equal(self.x_mean, old_x_mean) and self.almost_equal(self.y_mean, old_y_mean))

    def almost_equal(self, new_mean, old_mean):
        return abs(new_mean - old_mean) <= 0.00001

    def get_stats(self, all_points):
        # see for enlightenment: https://en.wikipedia.org/wiki/Precision_and_recall

        # number of points of this cluster which were correctly labeled to this cluster
        true_positives_number = len([point for point in self.points if point.given_label == self.label])
        # number of points of this cluster which were wrongly labeled to this cluster
        false_positives_number = len([point for point in self.points if point.given_label != self.label])
        # number of points from other clusters which were correctly excluded
        true_negatives_number = len([point for point in all_points
                                     if point.cluster.label != self.label and point.given_label != self.label])
        # number of points from other clusters which were missed
        false_negatives_number = len([point for point in all_points
                                      if point.cluster.label != self.label and point.given_label == self.label])

        accuracy = (true_positives_number + true_negatives_number) / len(all_points)
        precision = true_positives_number / (true_positives_number + false_positives_number)
        # the rappel from Assignment6.pdf
        recall = true_positives_number / (true_positives_number + false_negatives_number)
        score = 2 * precision * recall / (precision + recall)

        return Stats(self.label, accuracy, precision, recall, score)
