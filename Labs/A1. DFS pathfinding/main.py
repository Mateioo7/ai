import time
from random import randint

import pygame

from model.Drone import Drone
from model.Maps import Environment, DetectedMap

# Creating some colors
RED = (255, 0, 0)
GREEN = (0, 255, 0)
WHITE = (255, 255, 255)


# define a main function
def main():
    # we create the environment
    environment = Environment()
    environment.randomMap()
    # environment.load("resources/test2.map")
    # print(str(environment))

    # we create the map
    detected_map = DetectedMap()

    # initialize the pygame module
    pygame.init()
    # load and set the logo
    logo = pygame.image.load("resources/logo32x32.png")
    pygame.display.set_icon(logo)
    pygame.display.set_caption("Fănică's exploration journey")

    # we place the drone in a valid position
    x = randint(0, 19)
    y = randint(0, 19)
    while environment.surface[x][y] != 0:
        x = randint(0, 19)
        y = randint(0, 19)

    # create Fănică, the drone
    drone = Drone(x, y)

    # mark initial walls
    detected_map.mark_detected_walls(environment, x, y)

    # create a surface on screen that has the size of 800 x 480
    screen = pygame.display.set_mode((800, 400))
    screen.fill(WHITE)
    screen.blit(environment.get_image(), (0, 0))

    # define a variable to control the main loop
    running = True
    drone_finished = False

    # main loop
    while running:
        # if drone has not finished searching
        if not drone_finished:
            # make a move based on DFS algorithm
            drone.move_DFS(detected_map)
            # add time delay between each move
            time.sleep(0.1)
            # check if drone finished searching
            drone_finished = drone.x is None and drone.y is None
            # if drone has not finished searching
            if not drone_finished:
                # mark current walls and update gui display
                detected_map.mark_detected_walls(environment, drone.x, drone.y)
                screen.blit(detected_map.get_image(drone.x, drone.y), (400, 0))
                pygame.display.flip()
        # close game on clicking close button
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False

    pygame.quit()


# run the main function only if this module is executed as the main script
# (if you import this as a module then nothing is executed)
if __name__ == "__main__":
    # call the main function
    main()
