import pickle
from random import random

import numpy
import pygame

# define directions
UP = 2
DOWN = 0
LEFT = 1
RIGHT = 3
# creating some colors
WHITE = (255, 255, 255)
BLUE = (0, 0, 255)
BLACK = (0, 0, 0)
GRAY_BLUE = (50, 120, 120)


class Environment:
    def __init__(self):
        self._rows = 20
        self._columns = 20
        self._surface = numpy.zeros((self._rows, self._columns))

    @property
    def surface(self):
        return self._surface

    def randomMap(self, fill=0.2):
        for i in range(self._rows):
            for j in range(self._columns):
                if random() <= fill:
                    self._surface[i][j] = 1

    def __str__(self):
        string = ""
        for i in range(self._rows):
            for j in range(self._columns):
                string += str(int(self._surface[i][j]))
            string = string + "\n"
        return string

    def read_UDM_sensors(self, x, y):
        readings = [0, 0, 0, 0]
        # UP
        xf = x - 1
        while (xf >= 0) and (self._surface[xf][y] == 0):
            xf = xf - 1
            readings[UP] = readings[UP] + 1
        # DOWN
        xf = x + 1
        while (xf < self._rows) and (self._surface[xf][y] == 0):
            xf = xf + 1
            readings[DOWN] = readings[DOWN] + 1
        # LEFT
        yf = y + 1
        while (yf < self._columns) and (self._surface[x][yf] == 0):
            yf = yf + 1
            readings[RIGHT] = readings[RIGHT] + 1
        # RIGHT
        yf = y - 1
        while (yf >= 0) and (self._surface[x][yf] == 0):
            yf = yf - 1
            readings[LEFT] = readings[LEFT] + 1

        return readings

    def save(self, numFile):
        with open(numFile, 'wb') as f:
            pickle.dump(self, f)
            f.close()

    def load(self, numFile):
        with open(numFile, "rb") as f:
            dummy = pickle.load(f)
            self._rows = dummy.__n
            self._columns = dummy.__m
            self._surface = dummy.__surface
            f.close()

    def get_image(self):
        image = pygame.Surface((420, 420))
        brick = pygame.Surface((20, 20))
        brick.fill(BLUE)
        image.fill(WHITE)
        for i in range(self._rows):
            for j in range(self._columns):
                if self._surface[i][j] == 1:
                    image.blit(brick, (j * 20, i * 20))

        return image


class DetectedMap:
    def __init__(self):
        self._rows = 20
        self._columns = 20
        self._surface = self.get_init_surface()

    def get_init_surface(self):
        surface = numpy.zeros((self._rows, self._columns))
        for i in range(self._rows):
            for j in range(self._columns):
                surface[i][j] = -1
        return surface

    def mark_detected_walls(self, environment, x=17, y=5):
        walls = environment.read_UDM_sensors(x, y)
        i = x - 1
        if walls[UP] > 0:
            while (i >= 0) and (i >= x - walls[UP]):
                self._surface[i][y] = 0
                i = i - 1
        if i >= 0:
            self._surface[i][y] = 1

        i = x + 1
        if walls[DOWN] > 0:
            while (i < self._rows) and (i <= x + walls[DOWN]):
                self._surface[i][y] = 0
                i = i + 1
        if i < self._rows:
            self._surface[i][y] = 1

        j = y + 1
        if walls[RIGHT] > 0:
            while (j < self._columns) and (j <= y + walls[RIGHT]):
                self._surface[x][j] = 0
                j = j + 1
        if j < self._columns:
            self._surface[x][j] = 1

        j = y - 1
        if walls[LEFT] > 0:
            while (j >= 0) and (j >= y - walls[LEFT]):
                self._surface[x][j] = 0
                j = j - 1
        if j >= 0:
            self._surface[x][j] = 1

        return None

    def get_image(self, x, y):
        image = pygame.Surface((420, 420))
        brick = pygame.Surface((20, 20))
        empty = pygame.Surface((20, 20))
        empty.fill(WHITE)
        brick.fill(BLACK)
        image.fill(GRAY_BLUE)

        for i in range(self._rows):
            for j in range(self._columns):
                if self._surface[i][j] == 1:
                    image.blit(brick, (j * 20, i * 20))
                elif self._surface[i][j] == 0:
                    image.blit(empty, (j * 20, i * 20))

        drona = pygame.image.load("resources/drona.png")
        image.blit(drona, (y * 20, x * 20))
        return image

    @property
    def surface(self):
        return self._surface

    @property
    def rows(self):
        return self._rows

    @property
    def columns(self):
        return self._columns

    def __str__(self):
        string = ""
        for i in range(self._rows):
            for j in range(self._columns):
                if self._surface[i][j] == 0 or self._surface[i][j] == 1:
                    string += str(" " + str(int(self._surface[i][j])))
                else:
                    string += str(int(self._surface[i][j]))
            string = string + "\n"
        return string
