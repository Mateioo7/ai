import pygame


class Drone:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.path_so_far = []
        self.explored_coordinates = []

    def move(self, detected_map):
        pressed_keys = pygame.key.get_pressed()
        detected_map_surface = detected_map.surface
        if self.x > 0:
            if pressed_keys[pygame.K_UP] and detected_map_surface[self.x - 1][self.y] == 0:
                self.x = self.x - 1
        if self.x < 19:
            if pressed_keys[pygame.K_DOWN] and detected_map_surface[self.x + 1][self.y] == 0:
                self.x = self.x + 1
        if self.y > 0:
            if pressed_keys[pygame.K_LEFT] and detected_map_surface[self.x][self.y - 1] == 0:
                self.y = self.y - 1
        if self.y < 19:
            if pressed_keys[pygame.K_RIGHT] and detected_map_surface[self.x][self.y + 1] == 0:
                self.y = self.y + 1

    def move_DFS(self, detected_map):
        # go up if possible and unexplored yet
        if self.x > 0 and detected_map.surface[self.x - 1][self.y] == 0 and \
                [self.x - 1, self.y] not in self.explored_coordinates:
            self.path_so_far.append([self.x, self.y])
            self.explored_coordinates.append([self.x - 1, self.y])
            self.x -= 1
            return
        # go down if possible and unexplored yet
        if self.x < 19 and detected_map.surface[self.x + 1][self.y] == 0 and \
                [self.x + 1, self.y] not in self.explored_coordinates:
            self.path_so_far.append([self.x, self.y])
            self.explored_coordinates.append([self.x + 1, self.y])
            self.x += 1
            return
        # go left if possible and unexplored yet
        if self.y > 0 and detected_map.surface[self.x, self.y - 1] == 0 and \
                [self.x, self.y - 1] not in self.explored_coordinates:
            self.path_so_far.append([self.x, self.y])
            self.explored_coordinates.append([self.x, self.y - 1])
            self.y -= 1
            return
        # go right if possible and unexplored yet
        if self.y < 19 and detected_map.surface[self.x, self.y + 1] == 0 and \
                [self.x, self.y + 1] not in self.explored_coordinates:
            self.path_so_far.append([self.x, self.y])
            self.explored_coordinates.append([self.x, self.y + 1])
            self.y += 1
            return
        # turn back if possible
        if len(self.path_so_far) > 0:
            self.x, self.y = self.path_so_far.pop()
            return
        # finish searching, otherwise
        self.x = None
        self.y = None
