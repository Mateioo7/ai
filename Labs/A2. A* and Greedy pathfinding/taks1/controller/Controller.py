from heapq import *
from random import randint

import pygame

from taks1.model.Drone import Drone
from taks1.model.DronePosition import DronePosition
from taks1.utils.Utils import adjacent_squares, GREEN, RED, BLACK, GRAYBLUE


class Controller:
    def __init__(self, map):
        self._map = map
        self._drone = self.reset_drone()

    @property
    def map(self):
        return self._map

    @map.setter
    def map(self, value):
        self._map = value

    @property
    def drone(self):
        return self._drone

    @drone.setter
    def drone(self, value):
        self._drone = value

    def reset_drone(self):
        initial_x, initial_y = self.get_valid_random_coordinates()
        return Drone(initial_x, initial_y)

    def a_star_search(self, finalX, finalY):
        initial_position = DronePosition(self.drone.x, self.drone.y, None)
        final_position = DronePosition(finalX, finalY, None)

        positions_with_unvisited_neighbours = []
        positions_with_visited_neighbours = []

        # create the heap (priority queue) for taking the lowest total cost position
        heapify(positions_with_unvisited_neighbours)
        heappush(positions_with_unvisited_neighbours, initial_position)

        while len(positions_with_unvisited_neighbours) > 0:
            # we pop the drone position with the least total cost
            current_position = heappop(positions_with_unvisited_neighbours)

            # if the final position is reached, we return the reversed path
            if current_position == final_position:
                return self.a_star_reversed_path(current_position)

            neighbours_positions = []
            # we find the valid neighbours positions
            for adjacent_square_variation in adjacent_squares:
                neighbour_position = [current_position.x + adjacent_square_variation[0],
                                      current_position.y + adjacent_square_variation[1]]

                if self.valid_position(neighbour_position):
                    neighbours_positions.append(DronePosition(neighbour_position[0], neighbour_position[1],
                                                              current_position))

            for neighbour_position in neighbours_positions:
                # if position was already inspected, we skip it
                if neighbour_position in positions_with_visited_neighbours:
                    continue
                # we set the position's properties
                neighbour_position.distance_from_start = current_position.distance_from_start + 1
                neighbour_position.estimated_distance_to_end = self.manhattan_distance(neighbour_position.x,
                                                                                       neighbour_position.y,
                                                                                       finalX, finalY)
                neighbour_position.total_cost = neighbour_position.distance_from_start \
                                                + neighbour_position.estimated_distance_to_end
                # if position is still under inspection and the new total cost is greater than before, we skip it
                if len([position for position in positions_with_unvisited_neighbours
                        if neighbour_position == position and \
                           neighbour_position.distance_from_start > position.distance_from_start]):
                    continue

                # if the position was not inspected or was found a lower total cost, we add it for more inspection
                heappush(positions_with_unvisited_neighbours, neighbour_position)
            # we mark the current position as inspected
            positions_with_visited_neighbours.append(current_position)

        # no path is possible as a* algorithm guarantees the best possible solution if any solution exists
        return []

    def a_star_reversed_path(self, drone_position):
        path = []
        current_drone_position = drone_position
        while current_drone_position is not None:
            path.append([current_drone_position.x, current_drone_position.y])
            current_drone_position = current_drone_position.previous_drone_position
        return path[::-1]

    def valid_position(self, position):
        return 0 <= position[0] <= 19 and 0 <= position[1] <= 19 and \
               self._map.surface[position[0], position[1]] == 0

    def greedy_search(self, finalX, finalY):
        current_x, current_y = self.drone.x, self.drone.y
        # returns a list of moves as a list of pairs [x,y]
        # needed because when turning back some moves are lost and they don't show up in the gui window
        full_greedy_path = [[current_x, current_y]]
        greedy_path = [[current_x, current_y]]
        explored_coordinates = [[current_x, current_y]]


        while current_x != finalX or current_y != finalY:
            # 0 - up, 1 - down, 2 - left, 3 - right
            valid_options = {}
            if current_x > 0 and self._map.surface[current_x - 1, current_y] == 0 and \
                    [current_x - 1, current_y] not in explored_coordinates:
                valid_options[0] = self.manhattan_distance(current_x - 1, current_y, finalX, finalY)
            if current_x < 19 and self._map.surface[current_x + 1, current_y] == 0 and \
                    [current_x + 1, current_y] not in explored_coordinates:
                valid_options[1] = self.manhattan_distance(current_x + 1, current_y, finalX, finalY)
            if current_y > 0 and self._map.surface[current_x, current_y - 1] == 0 and \
                    [current_x, current_y - 1] not in explored_coordinates:
                valid_options[2] = self.manhattan_distance(current_x, current_y - 1, finalX, finalY)
            if current_y < 19 and self._map.surface[current_x, current_y + 1] == 0 and \
                    [current_x, current_y + 1] not in explored_coordinates:
                valid_options[3] = self.manhattan_distance(current_x, current_y + 1, finalX, finalY)

            if len(valid_options.items()) == 0:
                if len(greedy_path) > 0:
                    # working but no full greedy path
                    current_x, current_y = greedy_path.pop()
                # drone failed to find path
                elif len(greedy_path) == 0:
                    return []
                continue
            # dictionary.items() is a list of tuples: [(key1, value1), (key2, value2), ...]
            # best_option will contain the tuple with the minimum value
            best_option = min(valid_options.items(), key=lambda item: item[1])
            # 0 - up, 1 - down, 2 - left, 3 - right
            if best_option[0] == 0:
                current_x -= 1
                full_greedy_path.append([current_x, current_y])
                greedy_path.append([current_x, current_y])
                explored_coordinates.append([current_x, current_y])
            elif best_option[0] == 1:
                current_x += 1
                full_greedy_path.append([current_x, current_y])
                greedy_path.append([current_x, current_y])
                explored_coordinates.append([current_x, current_y])
            elif best_option[0] == 2:
                current_y -= 1
                full_greedy_path.append([current_x, current_y])
                greedy_path.append([current_x, current_y])
                explored_coordinates.append([current_x, current_y])
            elif best_option[0] == 3:
                current_y += 1
                full_greedy_path.append([current_x, current_y])
                greedy_path.append([current_x, current_y])
                explored_coordinates.append([current_x, current_y])

        return full_greedy_path

    def manhattan_distance(self, initialX, initialY, finalX, finalY):
        return abs(initialX - finalX) + abs(initialY - finalY)

    def dummy_search(self):
        # example of some path in test1.map from [5,7] to [7,11]
        return [[5, 7], [5, 8], [5, 9], [5, 10], [5, 11], [6, 11], [7, 11]]

    def get_valid_random_coordinates(self):
        x = randint(0, 19)
        y = randint(0, 19)

        while self._map.surface[x][y] == 1:
            x = randint(0, 19)
            y = randint(0, 19)

        return x, y

    def display_path_in_green(self, image, path):
        mark = pygame.Surface((20, 20))
        mark.fill(GREEN)
        for move in path:
            image.blit(mark, (move[1] * 20, move[0] * 20))

        # treating case in which origin and destination are the same
        if len(path) > 0:
            mark.fill(RED)
            destination = path[-1]
            image.blit(mark, (destination[1] * 20, destination[0] * 20))

            mark.fill(GRAYBLUE)
            origin = path[0]
            image.blit(mark, (origin[1] * 20, origin[0] * 20))

        return image

    def display_path_in_black(self, image, path):
        mark = pygame.Surface((20, 20))
        mark.fill(BLACK)
        for move in path:
            image.blit(mark, (move[1] * 20, move[0] * 20))

        # treating case in which origin and destination are the same
        if len(path) > 0:
            mark.fill(RED)
            destination = path[-1]
            image.blit(mark, (destination[1] * 20, destination[0] * 20))

            mark.fill(GRAYBLUE)
            origin = path[0]
            image.blit(mark, (origin[1] * 20, origin[0] * 20))

        return image
