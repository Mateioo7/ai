class DronePosition:
    def __init__(self, x, y, previous_drone_position=None):
        self.x = x
        self.y = y
        self.previous_drone_position = previous_drone_position

        self.total_cost = 0
        self.distance_from_start = 0
        self.estimated_distance_to_end = 0

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y

    def __lt__(self, other):
        return self.total_cost < other.total_cost

    def __gt__(self, other):
        return self.total_cost > other.total_cost
