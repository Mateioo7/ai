from taks1.controller.Controller import Controller
from taks1.gui.Gui import Gui
from taks1.model.Map import Map


def main():
    map = Map()
    # map.randomMap()
    map.loadMap("resources/test1.map")

    controller = Controller(map)
    gui = Gui(controller)
    gui.run()


if __name__ == "__main__":
    # call the main function
    main()
