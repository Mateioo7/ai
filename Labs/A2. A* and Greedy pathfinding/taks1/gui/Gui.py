import time

import pygame

from taks1.utils.Utils import WHITE


class Gui:
    def __init__(self, controller):
        self.controller = controller
        self.init_pygame()

    def init_pygame(self):
        # initialize the pygame module
        pygame.init()
        # load and set the logo
        logo = pygame.image.load("resources/logo32x32.png")
        pygame.display.set_icon(logo)
        pygame.display.set_caption("Path in simple environment")

    def run(self):
        screen = pygame.display.set_mode((400, 400))
        screen.fill(WHITE)

        running = True
        key_pressed = False

        final_x, final_y = -1, -1
        final_position_initiated = False
        while running:
            # event handling, gets all event from the event queue
            for event in pygame.event.get():
                # only do something if the event is of type QUIT
                if event.type == pygame.QUIT:
                    # change the value to False, to exit the main loop
                    running = False
                elif event.type == pygame.KEYDOWN:
                    key_pressed = True
                    # g - show greedy search path: gray blue is origin, red is destination
                    if event.key == pygame.K_g:
                        if not final_position_initiated:
                            final_x, final_y = self.controller.get_valid_random_coordinates()
                            final_position_initiated = True

                        greedy_search_start_time = time.time()
                        greedy_search_path = self.controller.greedy_search(final_x, final_y)
                        greedy_search_elapsed_time = time.time() - greedy_search_start_time

                        print("\n--- %.10f seconds elapsed for greedy search ---" % greedy_search_elapsed_time)
                        print("--- greedy path has " + str(len(greedy_search_path)) + " steps ---")

                        screen.blit(self.controller.display_path_in_green(self.controller.map.image(),
                                                                          greedy_search_path), (0, 0))
                        pygame.display.flip()
                    # a - show a star search path: gray blue is origin, red is destination
                    elif event.key == pygame.K_a:
                        if not final_position_initiated:
                            final_x, final_y = self.controller.get_valid_random_coordinates()
                            final_position_initiated = True

                        a_star_search_start_time = time.time()
                        a_star_search_path = self.controller.a_star_search(final_x, final_y)
                        a_star_search_elapsed_time = time.time() - a_star_search_start_time

                        print("\n--- %.10f seconds elapsed for a* search ---" % a_star_search_elapsed_time)
                        print("--- a* path has " + str(len(a_star_search_path)) + " steps ---")

                        screen.blit(self.controller.display_path_in_black(self.controller.map.image(),
                                                                          a_star_search_path), (0, 0))
                        pygame.display.flip()
                    # r - reset drone (origin) and destination
                    elif event.key == pygame.K_r:
                        self.controller.drone = self.controller.reset_drone()
                        final_x, final_y = self.controller.get_valid_random_coordinates()

            # if we haven't pressed any button, show the map with the drone
            if not key_pressed:
                screen.blit(self.controller.drone.mapWithDrone(self.controller.map.image()), (0, 0))
                pygame.display.flip()

        pygame.quit()
