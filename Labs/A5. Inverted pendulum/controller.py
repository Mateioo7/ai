from FuzzySet import FuzzySet
from Rule import Rule


class Controller:
    def solver(self, theta_angle, angular_speed):
        """
        Parameters
        ----------
        theta_angle : TYPE: float (O, x1)
            DESCRIPTION: the angle theta
        angular_speed : TYPE: float (w, x2)
            DESCRIPTION: the angular speed omega

        Returns
        -------
        F : TYPE: float (u)
            DESCRIPTION: the force that must be applied to the cart
        or

        None :if we have a division by zero
        """

        theta_angle_sets = [FuzzySet("NVB", -55, -40, -25), FuzzySet("NB", -40, -25, -10), FuzzySet("N", -20, -10, 0),
                            FuzzySet("Z", -5, 0, 5), FuzzySet("P", 0, 10, 20), FuzzySet("PB", 10, 25, 40),
                            FuzzySet("PVB", 25, 40, 55)]

        angular_speed_sets = [FuzzySet("NB", -13, -8, -3), FuzzySet("N", -6, -3, 0), FuzzySet("Z", -1, 0, 1),
                              FuzzySet("P", 0, 3, 6), FuzzySet("PB", 3, 8, 13)]

        rules = [Rule('PVVB', 'PVB', 'PB'), Rule('PVVB', 'PB', 'PB'), Rule('PVVB', 'PVB', 'P'),
                 Rule('PVB', 'P', 'PB'), Rule('PVB', 'PB', 'P'), Rule('PVB', 'PVB', 'Z'),
                 Rule('PB', 'Z', 'PB'), Rule('PB', 'P', 'P'), Rule('PB', 'PB', 'Z'), Rule('PB', 'PVB', 'N'),
                 Rule('P', 'N', 'PB'), Rule('P', 'Z', 'P'), Rule('P', 'P', 'Z'), Rule('P', 'PB', 'N'), Rule('P', 'PVB', 'NB'),
                 Rule('Z', 'NB', 'PB'), Rule('Z', 'N', 'P'), Rule('Z', 'Z', 'Z'), Rule('Z', 'P', 'N'), Rule('Z', 'PB', 'NB'),
                 Rule('N', 'NVB', 'PB'), Rule('N', 'NB', 'P'), Rule('N', 'N', 'Z'), Rule('N', 'Z', 'N'), Rule('N', 'P', 'NB'),
                 Rule('NB', 'NVB', 'P'), Rule('NB', 'NB', 'Z'), Rule('NB', 'N', 'N'), Rule('NB', 'Z', 'NB'),
                 Rule('NVB', 'NVB', 'Z'), Rule('NVB', 'NB', 'N'), Rule('NVB', 'N', 'NB'),
                 Rule('NVVB', 'NVB', 'N'), Rule('NVVB', 'NB', 'NB'), Rule('NVVB', 'NVB', 'NB')]

        force_discourse_values = {'NVVB': -32, 'NVB': -24, 'NB': -16, 'N': -8,
                                  'Z': 0, 'P': 8, 'PB': 16, 'PVB': 24, 'PVVB': 32}

        # we compute the membership degrees for current theta angle using triangular formula
        theta_angle_degrees = {set.name: 0 for set in theta_angle_sets}
        for set in theta_angle_sets:
            if self.value_inside_set(theta_angle, set):
                theta_angle_degrees[set.name] = self.triangular(theta_angle, set)

        # we compute the membership degrees for current angular speed using triangular formula
        angular_speed_degrees = {set.name: 0 for set in angular_speed_sets}
        for set in angular_speed_sets:
            if self.value_inside_set(angular_speed, set):
                angular_speed_degrees[set.name] = self.triangular(angular_speed, set)

        # we compute the membership degrees of current force according to the rules
        force_degrees = {rule.force_set_name: 0 for rule in rules}
        for rule in rules:
            force_degrees[rule.force_set_name] = max(
                force_degrees[rule.force_set_name],
                min(
                    theta_angle_degrees[rule.angle_set_name],
                    angular_speed_degrees[rule.speed_set_name]
                )
            )

        # we defuzzify the results of force membership degrees using a weighted average of the membership degrees
        # and the discourse values of the sets
        nominator = 0
        denominator = 0
        for force_key in force_degrees:
            nominator += force_degrees[force_key] * force_discourse_values[force_key]
            denominator += force_degrees[force_key]
        result = nominator / denominator if denominator != 0 else None

        return result

    def value_inside_set(self, value, set):
        return set.min <= value <= set.max

    def triangular(self, value, set):
        if set.middle - set.min == 0 or (set.max - set.middle) == 0:
            return 0

        return max(
            0,
            min(
                (value - set.min) / (set.middle - set.min),
                1,
                (set.max - value) / (set.max - set.middle)
            )
        )
