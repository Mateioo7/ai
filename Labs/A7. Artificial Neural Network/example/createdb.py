import torch
import numpy


def create_training_database():
    min = -10
    max = 10

    # create distribution of 1000 random pairs in range [-10, 10)
    pairs = (max - min) * torch.rand(1000, 2) + min

    x1: list[float] = [pair[0] for pair in pairs]
    x2: list[float] = [pair[1] for pair in pairs]

    x1: torch.Tensor = torch.tensor(x1)
    x2: torch.Tensor = torch.tensor(x2)

    function_results = torch.sin(x1 + (x2 / numpy.pi))

    # 3-dimensional tensor: (x1, x2, result)
    pairs_with_results = torch.column_stack((pairs, function_results))

    torch.save(pairs_with_results, "mydataset.dat")


create_training_database()
