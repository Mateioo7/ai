import torch


class Net(torch.nn.Module):
    # the class for the network

    def __init__(self, input_dimension, hidden_dimension, output_dimension):
        # we have two layers: a hidden one and an output one
        super(Net, self).__init__()
        """
            torch.nn.ReLU (Rectified Linear Unit)
        - given an input x, ReLU will take the maximal value between 0 and x.
        - basically makes 0 all the negative values
        - its role is to diversify the results
        
            torch.nn.Linear
        - applies a linear transformation to the incoming data: y = x*A^T + b
        """
        self.hidden = torch.nn.Linear(input_dimension, hidden_dimension)
        self.output = torch.nn.Sequential(
            torch.nn.Linear(input_dimension, hidden_dimension), torch.nn.ReLU(),
            torch.nn.Linear(hidden_dimension, hidden_dimension), torch.nn.ReLU(),
            torch.nn.Linear(hidden_dimension, output_dimension)
        )

    def forward(self, x):
        # defines the computation performed at every call
        return self.output(x)
